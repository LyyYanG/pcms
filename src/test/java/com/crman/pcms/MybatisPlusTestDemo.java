package com.crman.pcms;

import com.crman.pcms.entity.Hello;
import com.crman.pcms.mapper.HelloMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
// @AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class MybatisPlusTestDemo {
    @Autowired
    private HelloMapper helloMapper;
    @Test
    void testSelectAll() {
        Hello hello = new Hello();
        hello.setName("赵六");
        hello.setSex(1);
        hello.setAge(40);
        hello.setGrade("博士");
        helloMapper.insert(hello);

    }
}
