package com.crman.pcms;

import com.crman.pcms.entity.Hello;
import com.crman.pcms.mapper.HelloMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class SpringbootMybatisplusDemoApplicationTests {
    @Autowired
    private HelloMapper helloMapper;

    @Test
    public void testSelectList() {
        System.out.println(("----- selectAll method test ------"));
        List<Hello> userList = helloMapper.selectList(null);
        userList.forEach(System.out::println);
    }
}
