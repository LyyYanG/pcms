package com.crman.pcms.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

@Data
// @TableName
//表名与实体类名称不一致需要手动指定表名
@TableName("hello")
public class Hello implements Serializable {
    @TableId(type = IdType.AUTO)//雪花算法自增
    private Integer id;
    private String name;
    private Integer sex;
    private Integer age;
    private String grade;
}
