package com.crman.pcms;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackages = "com.crman.pcms.mapper")
public class PcmsApplication {

	public static void main(String[] args) {
		SpringApplication.run(PcmsApplication.class, args);
	}

}

