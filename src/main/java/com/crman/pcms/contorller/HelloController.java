package com.crman.pcms.contorller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@Tag(name = "hello")
public class HelloController {
    @GetMapping("/hello")
    @Operation(summary = "hello", description = "hello")
    public String hello() {
        log.info("testInfo");// 控制台打印日志
        return "hello";
    }
}